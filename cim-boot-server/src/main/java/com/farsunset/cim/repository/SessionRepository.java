package com.farsunset.cim.repository;

import com.farsunset.cim.sdk.server.model.CIMSession;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Repository;

import javax.annotation.Resource;
import java.util.List;
import java.util.Set;

/*
 * 正式场景下，使用redis或者数据库来存储session信息
 */
@Repository
public class SessionRepository {
    private final String PREFIX = "cim_session_";

    @Resource
    private RedisTemplate<String, CIMSession> redisTemplate;

    // private ConcurrentHashMap<String, CIMSession> map = new ConcurrentHashMap<>();

    public void save(CIMSession session) {
        // map.put(session.getAccount(), session);
        redisTemplate.opsForValue().set(PREFIX + session.getAccount(), session);
    }

    public CIMSession get(String account) {
        // return map.get(account);
        return redisTemplate.opsForValue().get(PREFIX + account);
    }

    public void remove(String account) {
        // map.remove(account);
        redisTemplate.delete(PREFIX + account);
    }

    public List<CIMSession> findAll() {
        // return new LinkedList<>(map.values());
        Set<String> keys =  redisTemplate.keys(PREFIX + "*");

        return redisTemplate.opsForValue().multiGet(keys);
    }
}
